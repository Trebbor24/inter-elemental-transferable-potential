# Inter-Elemental Transferable Potential

Source files for the IETP framework.

Training Neural Net:
	Upload whole IETP folder to your google drive under "My Drive/Colab Notebooks/".
	Open "manual_input.ipynb" with colaboratory and do pre-processing and training with the desired parameters.
	Datasets and trained models can be found in the corresponding folders (datasets may need to be downloaded first).
	You can use "manual_visualize_input.ipynb" to do some basic input data set visualization.